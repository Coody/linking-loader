//
//  main.c
//  linking loader
//
//  Created by Coody Chou on 2012/5/23.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long int charToNumber( char );

struct memory{
	char headerRecord ;
	char programName[7];
	char progStart[7];
	long int headerRecordStart ;
	char progLength[7];
	long int headerRecordLength ;
	char textStart[7];
	char textLength[3];
	long int textStartWithInt ;
	long int textLengthWithInt ;
	char memorySize[400000];
	char endRecordStart[7];
};

int main (int argc, const char * argv[])
{
	FILE *ptrRecord = fopen( "record.txt", "r");
	if( ptrRecord == NULL ){
		printf("ERROR!!!\n");
	}
	struct memory mainMemory ;
	int initialMatrix ;
	for( initialMatrix = 0 ; initialMatrix < 400000 ; initialMatrix++ ){
		mainMemory.memorySize[initialMatrix] = 'X';
	}
	while( !feof(ptrRecord) ){
		fscanf( ptrRecord , " %c%6s%6s%6s", &mainMemory.headerRecord , mainMemory.programName , mainMemory.progStart , mainMemory.progLength);
		if( feof(ptrRecord)){
            break;
        }
		mainMemory.headerRecordStart = charToNumber( mainMemory.progStart[1] )*65536 + charToNumber( mainMemory.progStart[2] )*4096 + charToNumber( mainMemory.progStart[3] )*256 + charToNumber( mainMemory.progStart[4] )* 16 + charToNumber( mainMemory.progStart[5] ) ;
		mainMemory.headerRecordLength = charToNumber( mainMemory.progLength[1] )*65536 + charToNumber( mainMemory.progLength[2] )*4096 + charToNumber( mainMemory.progLength[3] )*256 + charToNumber( mainMemory.progLength[4] )* 16 + charToNumber( mainMemory.progLength[5] ) ;
		mainMemory.headerRecordLength = mainMemory.headerRecordLength * 2 ;
		char tempChar ;
		do{
			fscanf( ptrRecord , " %c" , &tempChar );
			if( tempChar == 'T' ){
				fscanf( ptrRecord , " %6s%2s" , mainMemory.textStart , mainMemory.textLength  );
				mainMemory.textLengthWithInt = ( charToNumber(mainMemory.textLength[0])*16 + charToNumber(mainMemory.textLength[1]) ) * 2;
				mainMemory.textStartWithInt = charToNumber( mainMemory.textStart[1] )*65536 + charToNumber( mainMemory.textStart[2] )*4096 + charToNumber( mainMemory.textStart[3] )*256 + charToNumber( mainMemory.textStart[4] )* 16 + charToNumber( mainMemory.textStart[5] ) ;
				mainMemory.textStartWithInt =( mainMemory.textStartWithInt - mainMemory.headerRecordStart )* 2 + mainMemory.headerRecordStart ;
				long int ctrRun = mainMemory.textStartWithInt ;
				while( ctrRun < mainMemory.textStartWithInt + mainMemory.textLengthWithInt ){
					fscanf( ptrRecord , " %c" , &mainMemory.memorySize[ ctrRun ] );
					
					ctrRun++;
				}
			}
		}while( tempChar != 'E' );
		fscanf( ptrRecord , " %s" , mainMemory.endRecordStart );
	}
	
	
	int check = 0;
	printf("FIRST:\n");
	for( ; check + mainMemory.headerRecordStart < mainMemory.headerRecordStart+64 ; check++ ){
		if( check == 32 ){
			printf("\n");
		}
		printf( "%c" , mainMemory.memorySize[ check + mainMemory.headerRecordStart ] );
		
	}
	printf("\n\nMID:\n");
	long int checkMid = mainMemory.headerRecordLength / 2;
	for( check = 0 ; check + checkMid + mainMemory.headerRecordStart < checkMid + mainMemory.headerRecordStart+ 64  ; check++ ){
		if( check == 32 ){
			printf("\n");
		}
		printf("%c" , mainMemory.memorySize[ check + checkMid + mainMemory.headerRecordStart ] );
		
	}
	printf("\n\nLAST:\n");
	long int checkLast = mainMemory.headerRecordLength + mainMemory.headerRecordStart - 64 ;
	for( check = 0 ; check + checkLast < mainMemory.headerRecordLength + mainMemory.headerRecordStart ; check++ ){
		if( check == 32 ){
			printf("\n");
		}
		printf( "%c" , mainMemory.memorySize[ check + checkLast ] );
	}
	printf("\n");
	system("pause");
    return 0;
}

long int charToNumber(  char number ){
	long int tempNumber = 0;
	switch( number ){
		case '0':
			tempNumber = 0;
			break;
		case '1':
			tempNumber = 1;
			break;
		case '2':
			tempNumber = 2;
			break;
		case '3':
			tempNumber = 3;
			break;
		case '4':
			tempNumber = 4;
			break;
		case '5':
			tempNumber = 5;
			break;
		case '6':
			tempNumber = 6;
			break;
		case '7':
			tempNumber = 7;
			break;
		case '8':
			tempNumber = 8;
			break;
		case '9':
			tempNumber = 9;
			break;
		case 'A':
			tempNumber = 10;
			break;
		case 'B':
			tempNumber = 11;
			break;
		case 'C':
			tempNumber = 12;
			break;
		case 'D':
			tempNumber = 13;
			break;
		case 'E':
			tempNumber = 14;
			break;
		case 'F':
			tempNumber = 15;
			break;
		default:
			printf("Prog should not come HERE!!!\n");
			break;
	}
	return tempNumber ;
}
